import 'package:agora_rtm/agora_rtm.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_9.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Agora extends StatefulWidget {
  @override
  _AgoraState createState() => _AgoraState();
}

class _AgoraState extends State<Agora> {
  bool _isLogin = false;
  bool _isInChannel = false;

  final _userNameController = TextEditingController();
  final _channelNameController = TextEditingController();
  final _channelMessageController = TextEditingController();
  final _tokencontroller = TextEditingController();

  final _infoStrings = <String>[];
  String token;
  bool hastoken = false;
  gettoken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString("token") != null) {
      setState(() {
        token = prefs.getString("token");
        hastoken = true;
      });
    }
  }

  AgoraRtmClient _client;
  AgoraRtmChannel _channel;

  @override
  void initState() {
    super.initState();
    _createClient();
    gettoken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          children: [
            SizedBox(height: 50),
            _buildLogin(),
            Center(child: _token()),
            Center(child: _buildJoinChannel()),
            _gridlist(),
            _buildInfoList(),
            _buildSendChannelMessage(),
          ],
        ),
      )),
      debugShowCheckedModeBanner: false,
    );
  }

  Widget _gridlist() {
    if (!_isLogin || !_isInChannel) {
      return Container();
    }
    return Container(
      height: 100,
      child: FutureBuilder(
          future: _channel.getMembers(),
          builder: (context, snapshot) {
            if (snapshot.data == null) {}
            return GridView.builder(
              scrollDirection: Axis.horizontal,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 1,
              ),
              itemCount: snapshot.data.length,
              itemBuilder: (context, idx) {
                return Container(
                  decoration: BoxDecoration(
                      gradient:
                          LinearGradient(colors: [Colors.pink, Colors.purple]),
                      borderRadius: BorderRadius.all(Radius.circular(15))),
                  width: 10,
                  child: Center(child: Text(snapshot.data[idx].userId)),
                );
              },
            );
          }),
    );
  }

  void _createClient() async {
    _client =
        await AgoraRtmClient.createInstance("7558c9cab23a4dc6a8da64a54333d681");
    _client.onMessageReceived = (AgoraRtmMessage message, String peerId) {
      _log("Peer msg: " + peerId + ", msg: " + message.text);
    };
    _client.onConnectionStateChanged = (int state, int reason) {
      if (state == 5) {
        _client.logout();
        _log('Logout.');
        setState(() {
          _isLogin = false;
        });
      }
    };
  }

  Future<AgoraRtmChannel> _createChannel(String name) async {
    AgoraRtmChannel channel = await _client.createChannel(name);
    channel.onMemberJoined = (AgoraRtmMember member) {
      _log(
          "Member joined: " + member.userId + ', channel: ' + member.channelId);
    };
    channel.onMemberLeft = (AgoraRtmMember member) {
      _log("Member left: " + member.userId + ', channel: ' + member.channelId);
    };
    channel.onMessageReceived =
        (AgoraRtmMessage message, AgoraRtmMember member) {
      _log(member.userId + " : " + message.text);
    };
    return channel;
  }

  static TextStyle textStyle = TextStyle(fontSize: 18, color: Colors.blue);

  Widget _buildLogin() {
    return Row(children: <Widget>[
      _isLogin
          ? new Expanded(
              child: new Text('User Id: ' + _userNameController.text,
                  style: textStyle))
          : new Expanded(
              child: TextField(
                controller: _userNameController,
                decoration: InputDecoration(
                    hintText: 'User id here',
                    hintStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                    ),
                    filled: true,
                    fillColor: Color(0xffE7E7ED),
                    suffixIcon: IconButton(
                      icon: Icon(
                        Icons.send,
                        color: Color(0xFF0059D4),
                      ),
                      onPressed: _toggleLogin,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30.0)),
                      borderSide:
                          BorderSide(color: Color(0xffE7E7ED), width: 1.2),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(40.0)),
                        borderSide:
                            BorderSide(color: Color(0xffE7E7ED), width: 1.2))),
                textCapitalization: TextCapitalization.sentences,
              ),
            )
    ]);
  }

  Widget _token() {
    if (hastoken) {
      return Container();
    }
    return Center(
      child: Row(children: <Widget>[
        new Expanded(
          child: TextField(
            controller: _tokencontroller,
            decoration: InputDecoration(
                hintText: 'Token id in Agora.io',
                hintStyle: TextStyle(
                  color: Colors.black54,
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                ),
                filled: true,
                fillColor: Color(0xffE7E7ED),
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.send,
                    color: Color(0xFF0059D4),
                  ),
                  onPressed: () {
                    if (_tokencontroller.text.isNotEmpty) {
                      _sharedpref(_tokencontroller.text);
                    }
                  },
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(30.0)),
                  borderSide: BorderSide(color: Color(0xffE7E7ED), width: 1.2),
                ),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(40.0)),
                    borderSide:
                        BorderSide(color: Color(0xffE7E7ED), width: 1.2))),
            textCapitalization: TextCapitalization.sentences,
          ),
        ),
      ]),
    );
  }

  Widget _buildJoinChannel() {
    if (!_isLogin) {
      return Container();
    }
    return Row(children: <Widget>[
      _isInChannel
          ? new Expanded(
              child: new Text('Channel: ' + _channelNameController.text,
                  style: textStyle))
          : new Expanded(
              child: new TextField(
                  controller: _channelNameController,
                  decoration: InputDecoration(hintText: 'Input channel id'))),
      new OutlineButton(
        child: Text(_isInChannel ? 'Leave Channel' : 'Join Channel',
            style: textStyle),
        onPressed: _toggleJoinChannel,
      )
    ]);
  }

  Widget _buildSendChannelMessage() {
    if (!_isLogin || !_isInChannel) {
      return Container();
    }
    return Row(children: <Widget>[
      new Expanded(
          child: new TextField(
              controller: _channelMessageController,
              decoration: InputDecoration(hintText: 'Enter message'))),
      new OutlineButton(
        child: Icon(Icons.send),
        onPressed: _toggleSendChannelMessage,
      )
    ]);
  }

  Widget _buildInfoList() {
    return Expanded(
        child: Container(
            child: ListView.builder(
      itemBuilder: (context, i) {
        return ChatBubble(
          clipper: ChatBubbleClipper9(type: BubbleType.sendBubble),
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(top: 20),
          backGroundColor: Color(0xFF0059D4),
          child: Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width * 0.7,
            ),
            child: Text(
              _infoStrings[i],
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      },
      itemCount: _infoStrings.length,
    )));
  }

  void _sharedpref(String tokenis) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", tokenis);
    setState(() {
      token = tokenis;
      hastoken = true;
    });
  }

  void _toggleLogin() async {
    if (_isLogin) {
      try {
        await _client.logout();

        setState(() {
          _isLogin = false;
          _isInChannel = false;
        });
      } catch (errorCode) {}
    } else {
      String userId = _userNameController.text;
      if (userId.isEmpty) {
        return;
      }

      try {
        await _client.login(token, userId);
        setState(() {
          _isLogin = true;
        });
      } catch (errorCode) {}
    }
  }

  void _toggleJoinChannel() async {
    if (_isInChannel) {
      try {
        await _channel.leave();
        _log('Leave channel success.');
        _client.releaseChannel(_channel.channelId);
        _channelMessageController.text = null;

        setState(() {
          _isInChannel = false;
        });
      } catch (errorCode) {
        _log('Leave channel error: ' + errorCode.toString());
      }
    } else {
      String channelId = _channelNameController.text;
      if (channelId.isEmpty) {
        _log('Please input channel id to join.');
        return;
      }

      try {
        _channel = await _createChannel(channelId);
        await _channel.join();

        setState(() {
          _isInChannel = true;
        });
      } catch (errorCode) {
        _log('Join channel error: ' + errorCode.toString());
      }
    }
  }

  void _toggleSendChannelMessage() async {
    String text = _channelMessageController.text;
    if (text.isEmpty) {
      return;
    }
    try {
      await _channel.sendMessage(AgoraRtmMessage.fromText(text));
      _log(text);
      _channelMessageController.clear();
    } catch (errorCode) {}
  }

  void _log(String info) {
    print(info);
    setState(() {
      _infoStrings.insert(0, info);
    });
  }
}
