import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter/material.dart';
import 'home.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Agora',
      home: AnimatedSplashScreen(
          duration: 3000,
          splash: Container(
            height: 200,
            width: double.infinity,
            decoration: BoxDecoration(),
            child: Center(
              child: Text("AGORA"),
            ),
          ),
          nextScreen: Agora(),
          splashTransition: SplashTransition.fadeTransition,
          pageTransitionType: PageTransitionType.scale,
          backgroundColor: Color(0xFFFFFFFF)),
    );
  }
}
